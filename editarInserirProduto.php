<?php
include_once "class.tbProduto.php";
$produto = new produto;
if (isset($_REQUEST["funcao"])) {
  $funcao = $_REQUEST["funcao"];
}
if (isset($funcao)) {
  $produto->ID[0]         = $_REQUEST["id_prod"];
  $produto->NOME[0]       = $_REQUEST["nome_prod"];
  $produto->VALOR[0]      = $_REQUEST["valor"];
  $produto->QUANTIDADE[0] = $_REQUEST["quantidade"];
  $produto->editarInserir($funcao);
  header("location:listarProduto.php?id=".$id."&funcao=".$funcao);
}
else {
  $id = $_REQUEST["id_prod"];
  $funcao = isset($id) ? 'alterar' : "";
  if (isset($id)) {
    $filtro = "id_prod = ".$id;
    $produto->buscar($filtro);
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="styleCadastro.css" type="text/css"></link>
    <meta charset="utf-8">
    <title>Tela edicao ou insercao de produtos</title>
  </head>
  <body>
    <div id="wrap">
      <div id="conteudo">
        <form  name="editarInserirProduto" action="editarInserirProduto.php" method="post">
          <input type="hidden" name="funcao" value="<?php echo $funcao; ?>">
          <input type="hidden" name="id_prod" value="<?php echo $id; ?>">
          <table>
            <tr>
              <td>NOME: </td>
              <td><input type="text" name="nome_prod" maxlength="60" value="<?php echo ($id ? $produto->NOME[0] : "");?>"> </td>
            </tr>
            <tr>
              <td>VALOR: </td>
              <td><input type="text" name="valor" maxlength="23" value="<?php echo ($id ? $produto->VALOR[0] : "");?>"> </td>
            </tr>
            <tr>
              <td>QUANTIDADE: </td>
              <td><input type="text" name="quantidade" maxlength="6" value="<?php echo ($id ? $produto->QUANTIDADE[0] : "");?>"> </td>
            </tr>
            <tr>
    			    <td><input type="submit" name="salvar" value="Salvar" ></td>
            </tr>
            <tr>
              <td><a href="menu.html">Voltar</a></td>
            </tr>
          </table>
        </form>
      </div>
    </div>
  </body>
</html>
<?php
}
 ?>
