<?php
include_once "class.tbPessoa.php";
include_once "conexaoBanco.php";
$pessoa = new pessoa;
if(isset($_REQUEST["funcao"])){
	$funcao = $_REQUEST["funcao"];
}
if (isset($funcao)){
	$pessoa               = new pessoa();
  $option_tipo          = $_REQUEST["teste2"];
	$pessoa->ID[0]        = $_REQUEST["id"];
  $pessoa->ID_PESSOA[0] = $_REQUEST["id_pessoa"];
	$pessoa->NOME[0]      = $_REQUEST["nome"];
	$pessoa->CPF[0]       = $_REQUEST["cpf"];
	$pessoa->EMAIL[0]     = $_REQUEST["email"];
  $pessoa->SENHA[0]     = md5($_REQUEST["senha"]);
	$pessoa->editarInserir($funcao, $option_tipo);
  header("location:menu.html");
}
else{
  $id    	   = $_REQUEST["id"];
  $tipo      = $_REQUEST["tipo"];
	$id_pessoa = $_REQUEST["id_pessoa"];
	$funcao = isset($id) ? 'alterar' : "";
  if(isset($id)){
    if($tipo == "vendedor"){
    $filtro = "pessoa.id_pessoa) WHERE vendedor.id_vendedor=".$id;
    $pessoa->buscarVendedor($filtro);
    }
    else{
      $filtro = "pessoa.id_pessoa) WHERE cliente.id_cliente=".$id;
      $pessoa->buscarCliente($filtro);
    }
	}
?>
<!DOCTYPE html>
<html>
  <head>
		<link rel="stylesheet" href="styleLista.css" type="text/css"></link>
    <script
     src="https://code.jquery.com/jquery-3.1.0.min.js"
     integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
     crossorigin="anonymous"></script>
    <script type="text/javascript" src="funcoes.js"></script>
    <meta charset="utf-8">
    <title>Editar ou  Inserir Pessoa</title>
  </head>
  <body onload="onLoadFunctions()">
		<div id="wrap">
      <div id="conteudo">
		    <form name="teste" action="editarInserirPessoa.php" method="post">
		      <input id="id_pessoa" type="hidden" name="id_pessoa" value="<?php echo $pessoa->ID_PESSOA[0];?>">
		      <input type="hidden"  name="id"     value="<?php echo $pessoa->ID[0];?>">
		      <input type="hidden"  name="funcao" value="<?php echo $funcao;?>">
		      <input id="tipo" type="hidden"  name="tipo"   value="<?php echo $tipo;?>">
		      <table>
		        <tr>
		          <td>NOME: </td>
		          <td><input type="text" maxlength="60" name="nome" value="<?php echo ( $id ?  $pessoa->NOME[0] : "");?>"></td>
		        </tr>
		        <tr>
		          <td>EMAIL: </td>
		          <td><input type="text" maxlength="60" name="email" value="<?php echo ( $id ?  $pessoa->EMAIL[0] : "");?>"></td>
		        </tr>
		        <tr>
		          <td>CPF: </td>
		          <td><input type="text" maxlength="11" name="cpf" value="<?php echo ( $id ?  $pessoa->CPF[0] : "");?>"></td>
		        </tr>
		        <?php if ($_GET['tipo'] != 'cliente') { ?>
		        <tr id="senha">
		          <td >SENHA: </td>
		          <td><input type="password" maxlength="8" name="senha" value="<?php echo ( $id ?  $pessoa->SENHA[0] : "");?>"></td>
		        </tr>
		        <?php } ?>
		        <tr>
		          <td id="teste1">TIPO: </td>
		          <td>
		            <select id="teste2" name="teste2" onchange="escondeSenha(this.value)">
		              <option value="vendedor">Vendedor</option>
		              <option value="cliente">Cliente</option>
		            </select>
		          </td>
		        </tr>
		        <tr>
		          <td><input type="submit" name="salvar" value="Salvar"></td>
		        </tr>
						<tr>
							<td><a href="menu.html">Voltar</a></td>
						</tr>
		      </table>
		    </form>
			</div>
	</div>
  </body>
</html>
<?php

}
   ?>
