<?php
include_once "class.tbPessoa.php";
$pessoa = new pessoa();
$tipo = $_REQUEST["tipo"];
if ($tipo == "cliente"){
    $filtro = "pessoa.id_pessoa)";
    $pessoa->buscarCliente($filtro);
}
else {
    $filtro = "pessoa.id_pessoa)";
    $pessoa->buscarVendedor($filtro);
}

?>
  <!DOCTYPE html>
  <html>
   <head>
     <link rel="stylesheet" href="styleLista.css" type="text/css"></link>
     <script
           src="https://code.jquery.com/jquery-3.1.0.min.js"
           integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
           crossorigin="anonymous"></script>
         <script type="text/javascript" src="funcoes.js"></script>
         </script>
         <meta charset="utf-8">
         <title>Lista clientes</title>
     </head>
     <body>
        <div id="wrap">
          <div id="conteudo">
            <table id="tabLista">
              <tr>
                  <th>ID_<?php echo $tipo;?></th>
                  <th>NOME</th>
                  <th>EMAIL</th>
                  <th>CPF</th>
                  <th>FUNÇOES</th>
              </tr>
              <?php

              for($i=0; $i < count($pessoa->ID); $i++){
              ?>
                  <tr >
                      <td><?php echo $pessoa->ID[$i]; ?></td>
                      <td><?php echo $pessoa->NOME[$i]; ?></td>
                      <td><?php echo $pessoa->EMAIL[$i]; ?></td>
                      <td><?php echo $pessoa->CPF[$i]; ?></td>
                      <td><a href="editarInserirPessoa.php?id=<?php echo $pessoa->ID[$i];?>&id_pessoa=<?php echo $pessoa->ID_PESSOA[$i];?>&tipo=<?php echo $tipo;?>"</a>Editar</td>
                      <td><a onclick="confirmaExclusaoPessoa(<?php echo $pessoa->ID_PESSOA[$i];?>)" href="#">[X]</a></td>
                  </tr>
              <?php
              }
              ?>
            </table>
            <a href="menu.html">VOLTAR</a>
          </div>
      </div>
     </body>
  </html>
