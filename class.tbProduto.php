<?php
include_once "conexaoBanco.php";

class produto{
  public $ID           = array();
  public $NOME         = array();
  public $VALOR        = array();
  public $QUANTIDADE   = array();
  function editarInserir($funcao){
    global $db;
		if($funcao == "alterar"){
			$sql = "UPDATE produto
				         SET nome_prod       = '".$this->NOME[0]."',
      						   	   valor   		 = '".$this->VALOR[0]."',
      						   	   quantidade  = '".$this->QUANTIDADE[0]."'
      				 WHERE id_prod         = ".$this->ID[0];

		  $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro: ' . $db->ErrorMsg();
        exit;
      }
	  }
		else{
			$sql 	 = "INSERT INTO produto (nome_prod, valor, quantidade)
								     VALUES ('".$this->NOME[0]."',
								 		 '".$this->VALOR[0]."',
								 		 '".$this->QUANTIDADE[0]."')";
			$consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro: ' . $db->ErrorMsg();
        exit;
      }
		}
	}
  //funcao para buscar os produtos
  function buscar ($filtro){
    global $db;
    if($filtro){
      $sql = "SELECT id_prod,
                     nome_prod,
                     valor,
                     quantidade
                FROM produto
               WHERE ". $filtro;

      $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro: ' . $db->ErrorMsg();
        exit;
      }
      while($resposta = $consulta->fetchRow()){
          $this->ID[]         = $resposta["id_prod"];
          $this->NOME[]       = $resposta["nome_prod"];
          $this->VALOR[]      = $resposta["valor"];
          $this->QUANTIDADE[] = $resposta["quantidade"];
      }
    }
  }
  function excluir ($filtro){
    global $db;
    if($filtro){
        $sql = "DELETE FROM produto
                WHERE id_prod =".$filtro;

        $consulta = $db->execute($sql);
        if ($db->ErrorMsg()) {
          echo 'erro: ' . $db->ErrorMsg();
          exit;
        }
    }
  }

  function buscarQuantidade($filtro){
    global $db;
    $sql = "SELECT quantidade
              FROM produto
             WHERE id_prod = ".$filtro;             
    $consulta = $db->execute($sql);
    while($resposta = $consulta->fetchRow()){
      $this->QUANTIDADE[] = $resposta["quantidade"];
    }
  }

}
 ?>
