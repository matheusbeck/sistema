function escondeMostra(){
  if($('#id_pessoa').val() == '' || $('#id_pessoa').val() == null){
  	$('#teste1, #teste2').show();
  } else {
  	$('#teste1, #teste2').hide();
    //$('#teste2').select('cliente');
    $('#teste2 option[value=cliente]').attr('selected','selected');
  }
}

function escondeSenha(tipo){
  if(tipo == "cliente"){
    $('#senha').hide();
  }
  else{
    $('#senha').show();
  }
}

function confirmaExclusaoPessoa(id){
  var ret = confirm('Tem certeza?');
  if (ret == true) {
    $.post('excluirPessoa.php', {
    id_para_exclusao: id
  },  function(success) {
        if (success == "true") {
          alert('Excluido com sucesso!');
          location.reload();
        }
      else{
        alert('Erro!');
      }
      });
  }
}

function confirmaExclusaoProduto(id){
  var ret = confirm('Tem certeza?');
  if (ret == true) {
    $.post('excluirProduto.php', {
    id_para_exclusao: id
    },  function(success) {
          if (success == "true") {
            alert('Excluido com sucesso!');
            location.reload();
          }
          else{
            alert('Erro!');
          }
        });
  }}

function onLoadFunctions(tipo) {
  escondeMostra();
  escondeSenha(tipo);
}
