<?php
include_once "class.tbProduto.php";
$produto = new Produto();
$filtro  = "id_prod > 0";
$produto->buscar($filtro);


 ?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="styleLista.css" type="text/css"></link>
    <script
      src="https://code.jquery.com/jquery-3.1.0.min.js"
      integrity="sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s="
      crossorigin="anonymous"></script>
    <script type="text/javascript" src="funcoes.js"></script>
    </script>
    <meta charset="utf-8">
    <title>Produtos</title>
  </head>
  <body>
    <div id="wrap">
      <div id="conteudo">
        <table border="solid">
          <tr>
            <th>ID_PRODUTO</th>
            <th>NOME</th>
            <th>VALOR</th>
            <th>QUANTIDADE</th>
            <th>FUNCOES</th>
          </tr>
          <?php
            for ($i=0; $i < count($produto->ID); $i++){
          ?>
              <tr>
                <td><?php echo $produto->ID[$i]?></td>
                <td><?php echo $produto->NOME[$i]?></td>
                <td><?php echo $produto->VALOR[$i]?></td>
                <td><?php echo $produto->QUANTIDADE[$i]?></td>
                <td><a href="editarInserirProduto.php?id_prod=<?php echo $produto->ID[$i]; ?>">Editar</a></td>
                <td><a onclick="confirmaExclusaoProduto(<?php echo $produto->ID[$i];?>)" href="#">[X]</a></td>
              </tr>
          <?php
            }
          ?>
        </table>
        <a href="menu.html">Voltar</a>
      </div>
    </div>

  </body>
</html>
