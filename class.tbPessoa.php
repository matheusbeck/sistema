<?php
include_once "conexaoBanco.php";

// Active Record
class Pessoa {
  public $ID        = array();
  public $ID_PESSOA = array();
  public $NOME      = array();
  public $EMAIL     = array();
  public $CPF       = array();
  public $SENHA     = array();
  private $connection

  public function __construct(PDO $connection)
  {
    $this->connection = $connection;
  }

// FUNCAO PARA BUSCAR CLIENTES;
  public function buscarCliente($filtro) {
    global $db; // câncer

    if($filtro){
      $sql = "SELECT cliente.id_cliente,
                     pessoa.id_pessoa,
                     pessoa.nome_pessoa,
                     pessoa.email,
                     pessoa.cpf
                FROM pessoa
          INNER JOIN cliente ON (cliente.pessoa_id_pessoa = ".$filtro;
      $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro TbPessoa: ' . $db->ErrorMsg();
        exit;
      }
      while($resposta = $consulta->fetchRow()){
        $this->ID[]        = $resposta["id_cliente"];
        $this->ID_PESSOA[] = $resposta["id_pessoa"];
        $this->NOME[]      = $resposta["nome_pessoa"];
        $this->EMAIL[]     = $resposta["email"];
        $this->CPF[]       = $resposta["cpf"];
      }
    }
  }
    // FUNCAO PARA BUSCAR VENDEDORES;
  function buscarVendedor($filtro){
    global $db;
    if($filtro){
      $sql = "SELECT vendedor.id_vendedor,
                     pessoa.id_pessoa,
                     pessoa.nome_pessoa,
                     pessoa.email,
                     pessoa.cpf
                FROM pessoa
          INNER JOIN vendedor ON (vendedor.pessoa_id_pessoa = ".$filtro;
      $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro TbPessoa: ' . $db->ErrorMsg();
        exit;
      }
      while($resposta = $consulta->fetchRow()){
        $this->ID[]        = $resposta["id_vendedor"];
        $this->ID_PESSOA[] = $resposta["id_pessoa"];
        $this->NOME[]      = $resposta["nome_pessoa"];
        $this->EMAIL[]     = $resposta["email"];
        $this->CPF[]       = $resposta["cpf"];
        $this->SENHA[]     = $resposta["senha"];
      }
    }
  }
  // FUNCAO PARA INSERIR OU EDITAR OS VENDEDORES E CLIENTE;
  function editarInserir($funcao, $option_tipo){
    global $db;
    if($funcao == "alterar"){
      $sql = "UPDATE pessoa
                 SET nome_pessoa = '".$this->NOME[0]."',
                     email       = '".$this->EMAIL[0]."',
                     cpf         = '".$this->CPF[0]."'
               WHERE id_pessoa   =".$this->ID_PESSOA[0];
      $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
       echo 'erro TbPessoa: '.$db->ErrorMsg();
       exit;
      }
    }
    else if($option_tipo == "cliente"){
      $sql = "INSERT INTO pessoa (nome_pessoa, email,cpf)
                   VALUES ('".$this->NOME[0]."',
                               '".$this->EMAIL[0]."',
                               '".$this->CPF[0]."');
              INSERT INTO ".$option_tipo." (pessoa_id_pessoa)
                   VALUES ((SELECT max(id_pessoa)
                     FROM pessoa));";
      $inserir = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro TbPessoa: '.$db->ErrorMsg();
        exit;
      }
    }
    else {
      $sql = "INSERT INTO pessoa (nome_pessoa, email,cpf)
                   VALUES ('".$this->NOME[0]."',
                               '".$this->EMAIL[0]."',
                               '".$this->CPF[0]."');
              INSERT INTO ".$option_tipo." (pessoa_id_pessoa,senha)
                   VALUES ((SELECT max(id_pessoa)
                     FROM pessoa),'".$this->SENHA[0]."');";
      $inserir = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro TbPessoa: ' . $db->ErrorMsg();
        exit;
      }
    }
  }
    // FUNCAO PARA EXCLUIR VENDEDORES OU CLIENTES;
  function excluir($filtro){
    global $db;
    if($filtro){
      $sql = "DELETE
                FROM pessoa
               WHERE id_pessoa=".$filtro;
      $consulta = $db->execute($sql);
      if ($db->ErrorMsg()) {
        echo 'erro TbPessoa: ' . $db->ErrorMsg();
        exit;
      }
    }
  }
  function validaLogin($filtro){
    global $db;
    $sql = "SELECT pessoa.cpf, vendedor.senha, vendedor.id_vendedor
              FROM pessoa
        INNER JOIN vendedor ON (vendedor.pessoa_id_pessoa = pessoa.id_pessoa)
             WHERE ".$filtro;
    $consulta = $db->execute($sql);
    if ($db->ErrorMsg()) {
      echo 'erro TbPessoa: ' . $db->ErrorMsg();
      exit;
    }
    while($resposta = $consulta->fetchRow()){
        $this->CPF[]   = $resposta["cpf"];
        $this->SENHA[] = $resposta["senha"];
        $this->ID[]    = $resposta["id_vendedor"];
    }
  }
}
?>
