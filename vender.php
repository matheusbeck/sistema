<?php
include_once "class.tbPessoa.php";
include_once "class.tbProduto.php";
$id_vendedor = $_REQUEST["id_vendedor"];
$pessoa      = new pessoa();
$produto     = new produto();
$filtro      = "pessoa.id_pessoa)";
$filtro_prod  = "id_prod > 0";
$pessoa->buscarCliente($filtro);
$produto->buscar($filtro_prod);

 ?>

<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="styleLista.css" type="text/css"></link>
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <div id="wrap">
      <div id="conteudo">
          <form action="fechaVenda.php" method="post">
          <table>
            <tr>
              <td>Selecione o cliente:</td>
              <td>
                <select name="cliente">
                <?php
                for($i=0; $i < count($pessoa->ID); $i++){
                ?>
                  <option value="<?php echo $pessoa->ID[$i]?>"><?php echo $pessoa->NOME[$i]?></option>
                <?php
                }
                ?>
                </select>
              </td>
            </tr>
            <tr>
              <td>LISTA DE PRODUTOS</td>
            </tr>
            <tr>
              <table id="listaProd" border="solid">
                <tr>
                  <th>|_|</th>
                  <th>PRODUTO</th>
                  <th>PREÇO</th>
                  <th>QNT.</th>
                </tr>
                <?php
                for($i=0; $i < count($produto->ID); $i++){
                ?>
                    <input type="hidden" name="valor[<?php echo $produto->ID[$i]; ?>][]" value="<?php echo $produto->VALOR[$i]; ?>">
                    <input type="hidden" name="id_vendedor" value="<?php echo $id_vendedor; ?>">
                    <tr >
                        <td><input type="checkbox"  name="prod_selecionado[]" value="<?php echo $produto->ID[$i]; ?>"></td>
                        <td><?php echo $produto->NOME[$i]; ?></td>
                        <td><?php echo $produto->VALOR[$i]; ?></td>
                        <td><input type="text" name="quantidade[<?php echo $produto->ID[$i]; ?>][]" style="width:35px;"></td>

                    </tr>
                <?php
                }
                ?>
                <tr>
                  <td><input type="submit" name="submit_venda" id="submit_venda" value="Salvar"></td>
                </tr>
              </table>
        </form>
            </tr>
          <tr>
            <td><a href="menu.html">Voltar</a></td>
          </tr>
        </table>
      </div>
    </div>
  </body>
</html>
